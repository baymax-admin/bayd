# bayd

## 介绍

使用webpack构建的react组件库。简单便捷，按需引入。

## 安装

直接使用npm安装bayd
> npm i bayd

## 使用

支持按需加载，import自己需要的组件，则只加载该组件和样式。

```js
import React,{Component} from 'react'
import Button from 'bayd/lib/button'

export default class Welcome extends Component{
    render(){
        return(<div >
            <Button text='开始使用'/>
        </div>)
    }
}
```
