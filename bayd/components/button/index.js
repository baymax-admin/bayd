import React,{Component} from 'react'
import PropTypes from 'prop-types'
import Styles from './style.scss'

export default class Button  extends Component{
    static propTypes = {
    	text: PropTypes.string,
    	onClick: PropTypes.func
    }
    render(){
    	let {text} = this.props
    	return <div className={Styles['button-box']} onClick={this.props.onClick}>
    		{text}
    	</div>
    }
}