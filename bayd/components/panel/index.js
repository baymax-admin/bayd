import React,{Component} from 'react'
import PropTypes from 'prop-types'
import styles from './style.scss'

export default class Panel extends Component{
    static propTypes = {
    	name:PropTypes.string,
    	description:PropTypes.string,
    	renderContent:PropTypes.func,
    	renderParams:PropTypes.func
    }
    render(){
    	let {name,description,renderContent,renderParams} = this.props
    	return <div className={styles['wrap']}>
    		<div>
    			<h2>{name}</h2>
    		</div>
    		<div>
    			<h2>组件描述</h2>
    			<div>
    				{description}
    			</div>
    		</div>
    		<div className={styles['usage']}>
    			<h2>代码演示</h2>
    			<div className={styles['render-content']}>
    				{renderContent && renderContent()}
    			</div>
    		</div>
    		<div>
    			<h2>接入参数说明</h2>
    			{renderParams && renderParams()}
    		</div>
    	</div>
    }
}