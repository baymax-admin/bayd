import React,{Component} from 'react'
import PropTypes from 'prop-types'
export default class Chart extends Component{
	static propTypes = {
		canvasId: PropTypes.string,
		data: PropTypes.array
	}

	componentDidMount(){
	}
	drawCalibration(ctx,x0,y0,x1,y1,lineLength,lineCount,axis){
		if(!ctx) return
		// 开始绘制
		ctx.beginPath()
		// 绘制横纵坐标
		ctx.moveTo(x0, y0)
    	ctx.lineTo(x1, y1)
		// 绘制刻度
		if(axis=='x'){
			for(let i=0;i<lineCount;i++){
				ctx.moveTo(x0+30+30*i, y0)
				ctx.lineTo(x0+30+30*i, y0+lineLength)
			}
		}
		if(axis=='y'){
			for(let i=0;i<lineCount;i++){
				ctx.moveTo(x0, y0-30-30*i)
				ctx.lineTo(x0-lineLength, y0-30-30*i)
			}
		}
		
		ctx.stroke()
	}
	drawCanvas(canvasId,data){
		var canvas = document.getElementById(canvasId)
		if(!canvas) return
    	var ctx = canvas.getContext( '2d' )
		// 设置canvas大小和边框
    	canvas.width = 500
    	canvas.height = 500
    	canvas.style.border = '1px solid #000'
 
		this.drawCalibration(ctx,50,400,450,400,10,10,'x')
		this.drawCalibration(ctx,50,400,50,50,10,10,'y')
    	
		if(!data) return
    	data.forEach((item,index)=>{
    		//绘制矩形
    		ctx.beginPath()
			// x	矩形左上角的 x 坐标。
			// y	矩形左上角的 y 坐标。
			// width	矩形的宽度，以像素计。
			// height	矩形的高度，以像素计。
			// 简单来说，就是先选中一个点，然后拖动一定的宽高。
			// 正直就向右和向下，负值就向左和向上。
			// (初始x，初始y，向右拉动30，向上拉动30的倍数)
    		ctx.rect(50 + 30 + index*60, 400, 30, item * -30)
    		ctx.fillStyle = 'red'
    		ctx.fill()
    	})
	}

	render(){
		let {canvasId, data} = this.props
		this.drawCanvas(canvasId,data)
    	return<div>
    		<div id="container">
    			<canvas id={canvasId}>你的浏览器不支持canvas，请升级浏览器</canvas>
    		</div>
    	</div>
	}
}