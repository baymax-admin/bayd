import babel from 'rollup-plugin-babel'
import postcss from 'rollup-plugin-postcss'
  // resolve将我们编写的源码与依赖的第三方库进行合并
  import resolve from 'rollup-plugin-node-resolve';

export default {
    input:'./src/index.js',
    output:{
        file: './lib/index.js',
        format: 'esm',
        sourcemap: true
    },
    plugins:[
        resolve(),
        babel({ exclude: "**/node_modules/**", runtimeHelpers: true }),
        postcss({
            // extract: true,
            namedExports: true,
            sourceMap: true,
        })
    ],
    external:['react','react-dom']
}